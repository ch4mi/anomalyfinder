import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline, BSpline

def plot_waveform(event, S1orS2 = 's1', smooth = False):
 
    """Plots the requested pulse. Takes in a pandas dataframe row.""" 
    
    # Get pulse areas
    if S1orS2 == 's1':
        totPulseArea = event.S1c
    elif S1orS2 == 's2':
        totPulseArea = pow(10, event.logS2c)
    else:
        raise Exception("Pulse must be either 's1' or 's2'.")
        
    AFTdict = {'AFT1'  : 1,
               'AFT5'  : 5,
               'AFT10' : 10,
               'AFT25' : 25,
               'AFT50' : 50,
               'AFT75' : 75,
               'AFT90' : 90,
               'AFT95' : 95,
               'AFT99' : 99}
    
    # Prepend pulse type to make the appropriate RQ name
    AFTdict = {S1orS2 + AFT : val for AFT, val in AFTdict.items()}

    # Get AFT RQs
    AFTs = np.array(event[AFTdict.keys()]).astype(float)
    
    # AFT percentages
    AFTperc = [1, 5, 10, 25, 50, 75, 90, 95, 99]
    
    timesAFT = [0]
    areasAFT = [0]
    
    timesAFT.append(AFTs[0])
    areasAFT.append(AFTperc[0] * totPulseArea / 100 / AFTs[0])
    
    for i in range(len(AFTperc[1:])):
        timesAFT.append(AFTs[i+1])
        if (AFTs[i+1] - AFTs[i]):
            areasAFT.append((AFTperc[i+1] - AFTperc[i]) * totPulseArea / 100 / (AFTs[i+1] - AFTs[i]))
        else:
            areasAFT.append(areasAFT[-1])
            
    # Interpolate data for a smooth curve
    if smooth:
        timesAFT_new = np.linspace(timesAFT[0], timesAFT[-1], 30)
        spl = make_interp_spline(timesAFT, areasAFT, k = 3)
        areasAFT = spl(timesAFT_new)
        timesAFT = timesAFT_new
        
    plt.figure(figsize = (5,5))
    plt.plot(timesAFT, areasAFT, color = 'black')
    plt.xlabel('Time [ns]')
    plt.ylabel('Photons detected')
    plt.title('%s Pulse' % S1orS2)
    plt.show()